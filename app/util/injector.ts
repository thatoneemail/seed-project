import { EnvironmentPropertyLoader, FilePropertyLoader, PropertyDatabase } from '@fromnibly/config';
import { Container } from 'inversify';
import { InternalHandler } from '../handlers/internal';
import { PrometheusFactory } from './../factories/prometheus';
import { ErrorHandler } from './../handlers/error';
import { UtilHandler } from './../handlers/util';
import { Trap } from './trap';
import { types } from './types';

export async function create(): Promise<Container> {
  let container = new Container();

  let config = new PropertyDatabase((process.env['PROFILES'] || '').split(',').filter(x => x));
  config
    .withPropertyLoader(new EnvironmentPropertyLoader(process.env))
    .whichOverrides(new FilePropertyLoader('./config'));

  await config.loadProperties();

  container.bind(PropertyDatabase).toConstantValue(config);

  container
    .bind(types.Registries)
    .toDynamicValue(PrometheusFactory.createDefault)
    .inSingletonScope();

  let HttpRegistry = PrometheusFactory.create();
  container.bind(types.Registries).toConstantValue(HttpRegistry);
  container.bind(types.HttpRegistry).toConstantValue(HttpRegistry);

  container
    .bind<any>(types.Handlers)
    .to(UtilHandler)
    .inSingletonScope();
  container
    .bind<any>(types.Handlers)
    .to(InternalHandler)
    .inSingletonScope();

  //Error handler goes last
  container
    .bind<any>(types.Handlers)
    .to(ErrorHandler)
    .inSingletonScope();

  container
    .bind(Trap)
    .toSelf()
    .inSingletonScope();

  return container;
}
