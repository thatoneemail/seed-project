import * as bunyan from 'bunyan';

const level: any = process.env['LOG_LEVEL'] || 'info';

export function LOG(name: string, options: any = {}): bunyan {
  return bunyan.createLogger({
    name,
    level,
    ...options,
  });
}
