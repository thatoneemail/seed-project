require('reflect-metadata');
//spacer to keep reflect-metadata on top
import { PropertyDatabase } from '@fromnibly/config';
import { ClassRouter } from '@fromnibly/hallpass';
import { GracefulShutdownManager } from '@moebius/http-graceful-shutdown';
import * as express from 'express';
import { injectable, multiInject } from 'inversify';
import { create } from './util/injector';
import { LOG } from './util/logger';
import { Trap } from './util/trap';
import { types } from './util/types';

let logger = LOG('main');

@injectable()
class Main {
  constructor(
    private trap: Trap,
    @multiInject(types.Handlers) private handlers: any[],
    private config: PropertyDatabase
  ) {}

  main() {
    this.trap.init();

    let app = express();

    let router = new ClassRouter(app, LOG('ClassRouter'));

    for (let handler of this.handlers) {
      router.registerRouteHandler(handler);
    }

    router.initializeRoutes();

    let configuredPort = this.config.get('server.http.port').asNumber();
    let server = app.listen(configuredPort, () => {
      logger.info('server is started on port', configuredPort);
    });

    let shutdownManager = new GracefulShutdownManager(server);

    this.trap.addPrestopHook(() => {
      return new Promise<void>(resolve => {
        let shutdownWait = this.config.get('server.http.shutdown.wait');
        setTimeout(() => {
          resolve();
        }, shutdownWait.asNumber() * 1000);
      });
    });

    this.trap.addPrestopHook(() => {
      return new Promise<void>(resolve => {
        shutdownManager.terminate(() => {
          resolve();
        });
      });
    });
  }
}

create()
  .then(injector => {
    injector.bind(Main).toSelf();
    let main = injector.get(Main);

    return main.main();
  })
  .then(() => {
    logger.info('Initialized');
  })
  .catch(err => {
    logger.error(err, 'FATAL_ERROR');
    process.exit(1);
  });
