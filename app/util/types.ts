export const types = {
  Handlers: Symbol('Handlers'),
  Registries: Symbol('Registries'),
  HttpRegistry: Symbol('HttpRegistry'),
};
